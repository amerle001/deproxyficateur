import express from "express";
import { runUnproxy } from "./index";
import path from "path/posix";

const app = express();

app.use(express.json());

app.use(express.static('public'))
app.use(express.static(path.join(__dirname + '/../client/build/')))

const port = 5000;

app.get("/", (req, res) => {
res.sendFile(path.join(__dirname + '/../client/build/index.html'));
});


app.get("/api", async (req, res) => {
  try {
    const response = await runUnproxy();
    res.send(response);
  } catch (error) {
    res.send(error);
  }
});

app.listen(port, () => {
  console.log(`Serveur démarré sur le port ${port}`);
});
