import puppeteer from "puppeteer";

export interface Ressource {
  url: string | null;
  titre: string | null;
  description: string | null;
  imageUrl: string | null;
}

export const initRessourcesLinksMetadata = async () => {
  const publicAccessPages = [
    "https://bibliotheques.univ-pau.fr/fr/documentation/ressources-electroniques.html",
    "https://bibliotheques.univ-pau.fr/fr/documentation/ressources-electroniques/e-books.html",
    "https://bibliotheques.univ-pau.fr/fr/documentation/ressources-electroniques/dictionnaires.html",
  ];

  let ressourcesLink: (Ressource | null)[] = [];

  try {
    const browser = await puppeteer.launch({
      headless: true,
      args: ["--no-sandbox"],
    });
    const page = await browser.newPage();

    for (let url of publicAccessPages) {
      await page.goto(url);
      const rproxyLinks = await page.evaluate(() => {
        let arr: Ressource[] = [];
        let liens: (string | null)[] = Array.from(
          document.querySelectorAll(".link > .link-picture > a")
        ).map((link) => link.getAttribute("href"));

        let titres: (string | null)[] = Array.from(
          document.querySelectorAll(".link > .link-picture > a")
        ).map((link) => link.getAttribute("title"));

        let desc: (string | null)[] = Array.from(
          document.querySelectorAll(
            ".link > .link-infos > .link-description > p"
          )
        ).map((link) => link.textContent);

        let imgUrl: (string | null)[] = Array.from(
          document.querySelectorAll(".link > .link-picture > a > img")
        ).map((link) => link.getAttribute("src"));

        for (let i = 0; i < liens.length; i++) {
          arr.push({
            url: liens[i],
            titre: titres[i],
            description: desc[i] ? desc[i] : "",

            imageUrl: imgUrl[i]
              ? "https://bibliotheques.univ-pau.fr/" + imgUrl[i]
              : "rien",
          });
        }

        return arr;
      });

      ressourcesLink.push(...rproxyLinks);
    }
    await browser.close();
    return ressourcesLink;
  } catch (error) {
    console.log(error);
  }
};

const unproxify = (ressource: string): string => {
  /*
    1. prend chaque url et enlève ".rproxy.univ-pau.fr"
    2. si l'url continue après ".rproxy.univ-pau.fr, accole la fin à l'url déproxifiée
    3. nettoie la première partie de l'url en remplaçant les - par des . (si nécessaire)
    */

  const proxyStr = ".rproxy.univ-pau.fr";

  let urlSansTiret: string = "";

  // pour l'étape 1 et 2 (url completes, sans ".rproxy.univ-pau.fr")
  let urlStade1et2: string = "";

  // étape 3 : remplacer les . par des - (si nécessaire)

  const index = ressource.indexOf(proxyStr);

  // premier cas particulier : urls publiques sans ".rproxy.univ-pau.fr" sont renvoyées telles quelles
  if (index === -1) return ressource;

  const urlSansProxy = ressource.slice(0, index);
  const urlSansProxyNettoyee = urlSansProxy.replaceAll("-", ".");
  const tailString = ressource.slice(urlSansProxy.length + proxyStr.length);

  if (tailString.length > 0) {
    const urlComplete = urlSansProxyNettoyee + tailString;
    urlStade1et2 = urlComplete;
  } else {
    urlStade1et2 = urlSansProxyNettoyee;
  }

  // cas particuliers ou il y a vraiment des - dans le nom de domaine nettoyé (on les rétablit)
  if (urlStade1et2.includes("lefebvre")) {
    return "https://bibliotheque.lefebvre-dalloz.fr/";
  }
  if (urlStade1et2.includes("inis.cea.inist")) {
    return "https://inis-cea.inist.fr";
  }
  if (urlStade1et2.includes("alternatives.economiques.fr")) {
    return "https://www.alternatives-economiques.fr/";
  }
  if (urlStade1et2.includes("classiques.garnier.com")) {
    return "https://www.classiques-garnier.com";
  }
  if (urlStade1et2.includes("cobaz.afnor.org")) {
    return "https://cobaz.afnor.org";
  }
  if (urlStade1et2.includes("dalloz.actualite")) {
    return "https://www.dalloz-actualite.fr";
  }
  if (urlStade1et2.includes("univ-pau.dictionary.ent.reverso.net")) {
    return "https://univ-pau.dictionary.ent.reverso.net";
  }
  if (urlStade1et2.includes("frantext")) {
    return "https://www.frantext.fr";
  }
  if (urlStade1et2.includes("oecd.ilibrary.org")) {
    return urlStade1et2.replace("oecd.ilibrary.org", "oecd-ilibrary.org");
  }
  if (urlStade1et2.includes("techniques.ingenieur")) {
    return "https://www.techniques-ingenieur.fr";
  }
  if (urlStade1et2.includes("e.unwto.org")) {
    return "https://www.e-unwto.org/toc/unwtotfb/current";
  }

  // autres cas particuliers
  if (urlStade1et2.includes("search-alexanderstreet-com")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-alexanderstreet")) +
      ".alexanderstreet" +
      urlStade1et2.slice(
        urlStade1et2.indexOf("-alexanderstreet") + "-alexanderstreet".length
      );

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("nouveau-europresse-com")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-europresse")) +
      ".europresse" +
      urlStade1et2.slice(
        urlStade1et2.indexOf("-europresse") + "-europresse".length
      );

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("ascelibrary-org")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-org")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-org") + 1);
    // listeSansProxyNettoyee.push(url);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("proquest")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-proquest")) +
      ".proquest" +
      urlStade1et2.slice(
        urlStade1et2.indexOf("-proquest") + "-proquest".length
      );

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("afnor")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-afnor")) +
      ".afnor" +
      urlStade1et2.slice(urlStade1et2.indexOf("-afnor") + "-afnor".length);

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-org")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-org") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("bvdinfo")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-bvdinfo")) +
      ".bvdinfo" +
      urlStade1et2.slice(urlStade1et2.indexOf("-bvdinfo") + "-bvdinfo".length);

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("academic-oup")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-oup")) +
      ".oup" +
      urlStade1et2.slice(urlStade1et2.indexOf("-oup") + "-oup".length);

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("royalsociety-org")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-org")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-org") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("sciences-en-ligne-net")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-net")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-net") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("search-ebscohost-com")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-ebscohost")) +
      ".ebscohost" +
      urlStade1et2.slice(
        urlStade1et2.indexOf("-ebscohost") + "-ebscohost".length
      );

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("link-springer-com")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-springer")) +
      ".springer" +
      urlStade1et2.slice(
        urlStade1et2.indexOf("-springer") + "-springer".length
      );

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("access-torrossa-com")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-torrossa")) +
      ".torrossa" +
      urlStade1et2.slice(
        urlStade1et2.indexOf("-torrossa") + "-torrossa".length
      );

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
    urlSansTiret = urlStade1et2;
  } else if (urlStade1et2.includes("iopscience-iop-org")) {
    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-iop")) +
      ".iop" +
      urlStade1et2.slice(urlStade1et2.indexOf("-iop") + "-iop".length);

    urlStade1et2 =
      urlStade1et2.slice(0, urlStade1et2.indexOf("-org")) +
      "." +
      urlStade1et2.slice(urlStade1et2.indexOf("-org") + 1);
    urlSansTiret = urlStade1et2;
  } else {
    // début url : www- et fin -fr OU -com OU -net OU -info
    if (urlStade1et2[urlStade1et2.indexOf("www") + 3] === "-") {
      urlStade1et2 =
        urlStade1et2.slice(0, urlStade1et2.indexOf("www") + 3) +
        "." +
        urlStade1et2.slice(urlStade1et2.indexOf("www") + 4);

      if (urlStade1et2[urlStade1et2.indexOf("-fr")] === "-") {
        urlStade1et2 =
          urlStade1et2.slice(0, urlStade1et2.indexOf("-fr")) +
          "." +
          urlStade1et2.slice(urlStade1et2.indexOf("-fr") + 1);
      } else if (urlStade1et2[urlStade1et2.indexOf("-com")] === "-") {
        urlStade1et2 =
          urlStade1et2.slice(0, urlStade1et2.indexOf("-com")) +
          "." +
          urlStade1et2.slice(urlStade1et2.indexOf("-com") + 1);
      } else if (urlStade1et2[urlStade1et2.indexOf("-net")] === "-") {
        urlStade1et2 =
          urlStade1et2.slice(0, urlStade1et2.indexOf("-net")) +
          "." +
          urlStade1et2.slice(urlStade1et2.indexOf("-net") + 1);
      } else if (urlStade1et2[urlStade1et2.indexOf("-info")] === "-") {
        urlStade1et2 =
          urlStade1et2.slice(0, urlStade1et2.indexOf("-info")) +
          "." +
          urlStade1et2.slice(urlStade1et2.indexOf("-info") + 1);
      } else if (urlStade1et2[urlStade1et2.indexOf("-org")] === "-") {
        urlStade1et2 =
          urlStade1et2.slice(0, urlStade1et2.indexOf("-org")) +
          "." +
          urlStade1et2.slice(urlStade1et2.indexOf("-org") + 1);
      }
      urlSansTiret = urlStade1et2;
    } else {
      urlSansTiret = urlStade1et2;
    }
  }
  return urlSansTiret;
};

export const runUnproxy = async (): Promise<(Ressource | null)[]> => {
  let ressources = await initRessourcesLinksMetadata();
  console.log("Ressources");

  console.log(ressources);

  if (ressources === undefined) return [];
  for (let ressource of ressources) {
    if (ressource !== null && ressource.url !== null) {
      const urlDeproxifiee = unproxify(ressource.url);
      ressource.url = urlDeproxifiee;
    }
  }

  for (let i = 0; i < ressources.length; i++) {
    let flag = 0;
    for (let j = 0; j < ressources.length; j++) {
      if (ressources[i]?.url === ressources[j]?.url && i !== j) {
        flag++;
        if (flag > 0) {
          ressources.splice(j, 1);
          flag = 0;
        }
      }
    }
  }

  ressources.forEach((r) => console.log(r?.titre));

  return ressources.sort((a: Ressource | null, b: Ressource | null) => {
    if (a !== null && b !== null && a !== undefined && b !== undefined)
      return (
        (a?.titre?.trim().toLowerCase() as any) + b?.titre?.trim().toLowerCase()
      );
  });
};
