import "./App.css"
import Ressources from './components/Ressources';
import UPPA_logo from "./img/logo_uppa.png";

function App() {
  return (
    <div className="App">
      <header>

        <img src={UPPA_logo} className="UPPA-logo" alt="UPPA-logo" />
        <h1>Documentation électronique.</h1>
        <h2>Accès réservé aux lecteurs extérieurs du SCD de l'UPPA.</h2>
      </header>
      <Ressources />
    </div>
  );
}

export default App;
