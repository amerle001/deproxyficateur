import { useState, useEffect } from "react";

interface Ressource {
  url?: string | null;
  titre?: string | null;
  description?: string | null;
  imageUrl?: string | null;
}

const Ressources = () => {
  const [listeRessources, setListeRessources] = useState<Ressource[]>([]);
  useEffect(() => {
    fetch("/unproxy/api") // remove /unproxy/ in dev environment
      .then((res) => res.json())
      .then((data) => setListeRessources(data));
  }, []);
  return (
    <div>
      {listeRessources.length > 0 ? (
        <div>
          <p>
            Pour accéder aux ressources présentes sur cette page, ouvrez une
            session sur l'un des PC publics des BU.
          </p>{" "}
          {listeRessources.map((r) => (
            // Style temporaire à désactiver quand images sont sur nouveau site

            <div className="ressource-container" key={r.url}>
              <div className="temp-container">
                <img src={r.imageUrl ? r.imageUrl : undefined} />
              </div>
              <a href={r.url ? r.url : undefined} target="_blank">
                {r.titre}
              </a>
              <p>{r.description}</p>
            </div>
          ))}
        </div>
      ) : (
        <p>Récupération des liens en cours...</p>
      )}
    </div>
  );
};

export default Ressources;
